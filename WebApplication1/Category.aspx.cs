﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediAid
{
    public partial class category : System.Web.UI.Page
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["emp_id"] == null)
                Response.Redirect("Login.aspx");
        }

        public string displayData()
        {
            string data = "<h1>asdfasfd</h1>";
            return data;
        }

        protected void btnSubmit_click(object sender, EventArgs e) 
        {
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("insert_category", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@category_id", txtCategoryId.Text);
                cmd.Parameters.AddWithValue("@category_name", txtCategoryName.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
        }
        public string getData()
        {
            string data = null;
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("select_category", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    while (queryReader.Read())
                    {
                        int id = queryReader.GetInt32(0);
                        string category = queryReader.GetString(1);

                        data += "<tr class='odd pointer'>";
                        data += "<td class='a-center '>";
                        data += "</td>";
                        data += "<td>"+id+"</td>";
                        data += "<td class=''>"+category+"</td>";
                        data += "<td>";
                        data += "<a href='' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Edit </a>";
                        data += "<a href='' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> Delete </a>";
                        data += "</td>";
                        data += "</tr>";
                    }
                }
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
            return data;
        }
    }
}