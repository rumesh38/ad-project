﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MediAid.Login" %>

<!DOCTYPE html>

<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Medi-Aid | Login </title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login" style="background-image:url(Images/bg1.png); background-repeat: no-repeat; background-size: cover;">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper" style="color: white;">
        <div class="animate form login_form">
          <section class="login_content">
            <form runat="server">
              <h1 style="color:white;">Login Form</h1>
              <div>
                <asp:TextBox ID="txtUsrName" runat="server" type="text" class="form-control" placeholder="Username"/></asp:TextBox>
              </div>
              <div>
                <asp:TextBox ID="txtUsrPass" runat="server" type="password" class="form-control" placeholder="Password" required="" /></asp:TextBox>
              </div>
                <div>
                <%--<button class="btn btn-round btn-primary bg-orange" >Log in</button>
                  <asp:Button ID="btnSubmit" runat="server" Text="Add" type="submit" class="btn btn-success" OnClick="btnSubmit_Click" />--%>
                <asp:Button ID="btnLogin" runat="server" Text="Log in" type="submit" class="btn btn-round btn-primary bg-orange" OnClick="btnLogin_Click"/>
                <%--<a class="reset_pass" href="#" style="color: white;">Lost your password?</a>--%>
              
                </div>

              <div class="clearfix"></div>

              <div class="separator">
                <%--<p class="change_link" style="color: black;">New to site?
                  <a href="#signup" class="to_register" style="color: white;"> Create Account </a>
                </p>--%>

                <div class="clearfix"></div>
                <br />

                <div>
                  
                  <h1 style="color: skyblue;"><i><img src="Images/HEALTH_LOGO_DESIGN.png" style="height:65px; width: 65px;" /></i> MediAid</h1>
                  <p style="color: grey;">©2018 All Rights Reserved. MediAid - Stock Management System</p></div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form ">
          <section class="login_content">
            <form>
              <h1 style="color: white;">Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-round btn-success bg-blue-sky" href="index.html">Submit</a>
              </div>

              <div class="clearfix"></div>


              <div class="separator">
                <p class="change_link" style="color:black;">Already a member ?
                  <a href="#signin" class="to_register" style="color: white;"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                   <h1 style="color: skyblue;"><i><img src="Images/HEALTH_LOGO_DESIGN.png" style="height:65px; width: 65px;" /></i> MediAid</h1>
                  <p style="color: grey;">©2018 All Rights Reserved. MediAid - Stock Management System</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>

