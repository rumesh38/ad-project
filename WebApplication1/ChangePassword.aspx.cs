﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediAid
{
    public partial class ChangePassword : System.Web.UI.Page
    {
         string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

         public string getData()
        {
            string data = null;
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("select_employee", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    while (queryReader.Read())
                    {
                        int id = queryReader.GetInt32(0);
                        string employee_name = queryReader.GetString(1);
                        string username = queryReader.GetString(2);
                        string password = queryReader.GetString(3);

                        data += "<tr class=\"odd pointer\">";
                        data += "<td class=\"a-center \"></td>";
                        data += "<td>" + id + "</td>";
                        data += "<td>" + employee_name + "</td>";
                        data += "<td>" + username + "</td>";
                        data += "<td>" + password + "</td>";

                        data+="<td>";
                        data+="<a href='./EditCustomer.aspx?id="+id+"' class=\"btn btn-info btn-xs\"><i class=\"fa fa-pencil\"></i>Edit </a>";
                        data += "<a href='./DeleteCustomer.aspx?id=" + id + "' class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash-o\"></i>Delete </a>";
                        data+="</td>";
                        data+="</tr>";
                    }
                }
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
            return data;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("update_emp_pw", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", txtempId.Text);
                cmd.Parameters.AddWithValue("@employee_name", txtempName.Text);
                cmd.Parameters.AddWithValue("@username", empun.Text);
                cmd.Parameters.AddWithValue("@password", emppw.Text);
                
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
                reset();
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Delete')</script>");
        }

        public void reset()
        {
            txtempId.Text = (Int32.Parse(txtempId.Text)+1).ToString();
            txtempName.Text = "";
            empun.Text = "";
            emppw.Text = "";
        }
    }
}
    
