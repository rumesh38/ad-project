﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediAid
{
    public partial class customer : System.Web.UI.Page
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["emp_id"] == null)
                Response.Redirect("Login.aspx");
        }
        public string getData()
        {
            string data = null;
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("select_customer", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    while (queryReader.Read())
                    {
                        int id = queryReader.GetInt32(0);
                        string customer_name = queryReader.GetString(1);
                        string customer_address = queryReader.GetString(2);
                        string customer_phone = queryReader.GetString(3);
                        string customer_email = queryReader.GetString(4);

                        data += "<tr class=\"odd pointer\">";
                        data += "<td class=\"a-center \"></td>";
                        data += "<td>" + id + "</td>";
                        data += "<td>" + customer_name + "</td>";
                        data += "<td>" + customer_address + "</td>";
                        data += "<td>" + customer_phone + "</td>";
                        data += "<td>" + customer_email + "</td>";
                        data+="<td>";
                        data+="<a href='./EditCustomer.aspx?id="+id+"' class=\"btn btn-info btn-xs\"><i class=\"fa fa-pencil\"></i>Edit </a>";
                        data += "<a href='./DeleteCustomer.aspx?id=" + id + "' class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash-o\"></i>Delete </a>";
                        data+="</td>";
                        data+="</tr>";
                    }
                }
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
            return data;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("insert_customer", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customer_id", customerid.Text);
                cmd.Parameters.AddWithValue("@customer_name", customername.Text);
                cmd.Parameters.AddWithValue("@customer_address", customeradd.Text);
                cmd.Parameters.AddWithValue("@customer_phone", customerphone.Text);
                cmd.Parameters.AddWithValue("@customer_email", customeremail.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
                reset();
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Delete')</script>");
        }

        public void reset()
        {
            customerid.Text = (Int32.Parse(customerid.Text)+1).ToString();
            customername.Text = "";
            customeradd.Text = "";
            customerphone.Text = "";
            customeremail.Text = "";
        }
    }
}