﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediAid
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public int getLowOnStock()
        {
            int count = 0;
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("select_low_on_stock", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    while (queryReader.Read())
                    {
                        count++;
                    }
                }
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }

            return count;
        }

        public int getExpiredItems()
        {
            int count=0;
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("select_expired_items", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    while (queryReader.Read())
                    {
                        count++;
                    }
                }
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
            return count;
        }
    }
}