﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediAid
{
    public partial class Sales : System.Web.UI.Page
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["emp_id"] == null)
                Response.Redirect("Login.aspx");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Button clicked");
            if (checkProductValidity(Int32.Parse(product_id.Text), Int32.Parse(quantity.Text)))
            {
                SqlConnection mysqlConn = new SqlConnection(connectionString);
                try
                {
                    mysqlConn.Open();
                    SqlCommand cmd = new SqlCommand("insert_sales", mysqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@customer_id", customer_id.Text);
                    cmd.Parameters.AddWithValue("@emp_id", 1);
                    cmd.Parameters.AddWithValue("@product_id", product_id.Text);
                    cmd.Parameters.AddWithValue("@sales_quantity", quantity.Text);
                    cmd.Parameters.AddWithValue("@billing_date", single_cal2.Text);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                catch (SqlException ex)
                {
                    Response.Redirect("Sales.aspx");
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    mysqlConn.Close();
                    //reset();
                }

                Response.Write("<script>alert('Sales successfully Created.')</script>");
            }
            else
            {
                Response.Write("<script>alert('Product out of limit')</script>");
            }
        }

        private Boolean checkProductValidity(int id, int qty)
        {
            Boolean res = false;
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("select_product_by_id", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@product_id", product_id.Text);
                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    queryReader.Read();
                    int prod_qty = queryReader.GetInt32(8);
                    //Console.WriteLine("Total quantity : " + prod_qty);
                    if (prod_qty >= qty)
                    {
                        res = true;
                    }
                }
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
            return res;
        }

        public string getData()
        {
            string data = null;
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("select_sales", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    while (queryReader.Read())
                    {
                        int id = queryReader.GetInt32(0);
                        string customer_name = queryReader.GetString(2);
                        string emp_name = queryReader.GetString(4);
                        string product_name = queryReader.GetString(6);
                        int sales_price = queryReader.GetInt32(8);
                        int sales_quantity = queryReader.GetInt32(9);
                        string billing_date = queryReader.GetDateTime(10).ToString();

                        data += "<tr class=\"odd pointer\">";
                        data += "<td class=\"a-center \"></td>";
                        data += "<td>" + id + "</td>";
                        data += "<td>" + emp_name + "</td>";
                        data += "<td>" + customer_name + "</td>";
                        data += "<td>" + billing_date + "</td>";
                        data += "<td>" + product_name + "</td>";
                        data += "<td>" + sales_price + "</td>";
                        data += "<td>" + sales_quantity + "</td>";
                        data += "<td>" + (sales_price*sales_quantity) + "</td>";
                        data += "<td>";
                        data += "<a href='./DeleteSales.aspx?id=" + id + "' class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash-o\"></i>Delete </a>";
                        data += "</td>";
                        data += "</tr>";
                    }
                }
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
            return data;
        }
    }
}