﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediAid
{
    public partial class DeleteCustomer : System.Web.UI.Page
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        int customer_id = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {

                SqlConnection mysqlConn = new SqlConnection(connectionString);
                try
                {
                    customer_id = Int32.Parse(Request.QueryString["id"].ToString());
                    mysqlConn.Open();
                    SqlCommand cmd = new SqlCommand("delete_customer", mysqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", customer_id);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    Response.Redirect("Customer.aspx", true);
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);

                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    mysqlConn.Close();
                }
            }
            else
            {
                // Show error for no id found
            }
        }
    }
}