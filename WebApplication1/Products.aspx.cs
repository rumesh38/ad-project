﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediAid
{
    public partial class item : System.Web.UI.Page
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["emp_id"] == null)
                Response.Redirect("Login.aspx");
        }

        public string displayData()
        {
            string data = "<h1>asdfasfd</h1>";
            return data;
        }

        protected void btnSubmit_click(object sender, EventArgs e)
        {
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("insert_product", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@product_id", txtProID.Text);
                cmd.Parameters.AddWithValue("@product_name", txtProName.Text);
                cmd.Parameters.AddWithValue("@price", txtPrice.Text);
                cmd.Parameters.AddWithValue("@category_id", txtCatID.Text);
                cmd.Parameters.AddWithValue("@emp_id", txtEmpID.Text);
                cmd.Parameters.AddWithValue("@purchase_date", txtPurchase_date.Text);
                cmd.Parameters.AddWithValue("@manufacture_date", txtManufacture_date.Text);
                cmd.Parameters.AddWithValue("@expiry_date", txtExpiry_date.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
        }
        public string getData()
        {
            string data = null;
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("select_product_only", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    while (queryReader.Read())
                    {
                        int product_id = queryReader.GetInt32(0);
                        string product_name = queryReader.GetString(1);
                        int price = queryReader.GetInt32(2);
                        int category_id = queryReader.GetInt32(3);
                        int emp_id = queryReader.GetInt32(4);
                        DateTime purchase_date = queryReader.GetDateTime(5);
                        DateTime manufacture_date = queryReader.GetDateTime(6);
                        DateTime expiry_date = queryReader.GetDateTime(7);

                        data += "<tr class='odd pointer'>";
                        data += "<td class='a-center '>";
                        data += "</td>";
                        data += "<td>" + product_id + "</td>";
                        data += "<td class=''>" + product_name + "</td>";
                        data += "<td class=''>" + price + "</td>";
                        data += "<td class=''>" + category_id + "</td>";
                        data += "<td class=''>" + emp_id + "</td>";
                        data += "<td class=''>" + purchase_date + "</td>";
                        data += "<td class=''>" + manufacture_date + "</td>";
                        data += "<td class=''>" + expiry_date + "</td>";
                        data += "<td>";
                        data += "<a href='' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Edit </a>";
                        data += "<a href='' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> Delete </a>";
                        data += "</td>";
                        data += "</tr>";
                    }
                }
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
            return data;
        }
    }
}