﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediAid
{
    public partial class Suppliers : System.Web.UI.Page
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["emp_id"] == null)
                Response.Redirect("Login.aspx");
        }

        public string displayData()
        {
            string data = "<h1>asdfasfd</h1>";
            return data;
        }

        public string getData()
        {
            string data = null;
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("select_supplier", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    while (queryReader.Read())
                    {
                        int supplier_id = queryReader.GetInt32(0);
                        string supplier_name = queryReader.GetString(1);
                        string supplier_address = queryReader.GetString(2);
                        int emp_id = queryReader.GetInt32(3);
                        string company_name = queryReader.GetString(4);
                        string contact_no = queryReader.GetString(5);
                        string url = queryReader.GetString(6);

                        data += "<tr class='odd pointer'>";
                        data += "<td class='a-center '>";
                        data += "</td>";
                        data += "<td>" + supplier_id + "</td>";
                        data += "<td class=''>" + supplier_name + "</td>";
                        data += "<td class=''>" + supplier_address + "</td>";
                        data += "<td" + emp_id + "</td>";
                        data += "<td class=''>" + company_name + "</td>";
                        data += "<td class=''>" + contact_no + "</td>";
                        data += "<td class=''>" + url + "</td>";
                        data += "<td>";
                        data += "<a href='' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Edit </a>";
                        data += "<a href='' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> Delete </a>";
                        data += "</td>";
                        data += "</tr>";
                    }
                }
                cmd.Dispose();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
            return data;
        }

        protected void btnSubmit_click(object sender, EventArgs e)
        {
            SqlConnection mysqlConn = new SqlConnection(connectionString);
            try
            {
                mysqlConn.Open();
                SqlCommand cmd = new SqlCommand("insert_supplier", mysqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@supplier_id", txtSupID.Text);
                cmd.Parameters.AddWithValue("@supplier_name", txtSupName.Text);
                cmd.Parameters.AddWithValue("@supplier_address", txtSupAddress.Text);
                cmd.Parameters.AddWithValue("@emp_id", txtEmpID.Text);
                cmd.Parameters.AddWithValue("@supplier_number", txtSupCont.Text);
                cmd.Parameters.AddWithValue("@company_name", txtCompName.Text);
                cmd.Parameters.AddWithValue("@contact_no", txtContNo.Text);
                cmd.Parameters.AddWithValue("@url", txtUrl.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();                
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysqlConn.Close();
            }
        }
        
    }
}