﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediAid
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["emp_id"] = null;
            Session["name"] = null;
            Session["emp_type"] = null;
            Response.Redirect("Login.aspx");
        }
    }
}