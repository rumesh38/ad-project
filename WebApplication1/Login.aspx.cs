﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace MediAid
{
    public partial class Login : System.Web.UI.Page
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["emp_id"] != null)
            {
                //if(Session["emp_type"].Equals("admin"))
                //    Response.Redirect("AdminDashboard.aspx");
                //else
                    //Response.Redirect("Default.aspx");
                if(Session["emp_id"]!=null)
                    Response.Redirect("Default.aspx");
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string constr = WebConfigurationManager.ConnectionStrings["conn"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("login_check",con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@username", txtUsrName.Text);
                cmd.Parameters.AddWithValue("@password", txtUsrPass.Text);
                SqlDataReader queryReader = cmd.ExecuteReader();
                if (queryReader.HasRows)
                {
                    queryReader.Read();
                    //emp_id, emp_name, username, email, phone, emp_type
                    int emp_id = queryReader.GetInt32(0);
                    string emp_name = queryReader.GetString(1);
                    string emp_type = queryReader.GetString(5);
                    Session["emp_id"] = emp_id;
                    Session["name"] = emp_name;
                    Session["emp_type"] = emp_type;
                    //if (emp_type.Equals("admin"))
                    //{
                    //    Response.Redirect("AdminDashboard.aspx");
                    //    Response.Write("<script>alert('Success!')</script>");
                    //}
                    //else
                    //{
                        Response.Redirect("Default.aspx");
                        Response.Write("<script>alert('Success!')</script>");
                    //}
                }
                else
                {
                    Response.Write("<script>alert('error!')</script>");
                }
                cmd.Dispose();

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

    }
}